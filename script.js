"use strict";

const possibilities = ["rock", "paper", "scissors"];

const optComputer =
  possibilities[Math.floor(Math.random() * possibilities.length)];

const optYou = possibilities[Math.floor(Math.random() * possibilities.length)];

function play() {
  if (optComputer === optYou) {
    console.log(
      `Computer choice: ${optComputer}   User choice: ${optYou}  It's a draw!`
    );
  } else if (optComputer === "rock" && optYou === "paper") {
    console.log(`Computer choice : "Rock"   User choice: “Paper”  User wins!`);
  } else if (optComputer === "rock" && optYou === "scissors") {
    console.log(
      `Computer choice : "Rock"   User choice: “Scissors”  Computer wins!`
    );
  } else if (optComputer === "paper" && optYou === "rock") {
    console.log(
      `Computer choice : "Paper   User choice: “Rock”  Computer wins!`
    );
  } else if (optComputer === "paper" && optYou === "scissors") {
    console.log(
      `Computer choice : "Paper"   User choice: “Scissors”  User wins!`
    );
  } else if (optComputer === "scissors" && optYou === "rock") {
    console.log(
      `Computer choice : "Scissors"   User choice: “Rock”  User wins!`
    );
  } else if (optComputer === "scissors" && optYou === "paper") {
    console.log(
      `Computer choice : "Scissors"   User choice: “Paper”  Computer wins!`
    );
  }
}
// please open the console to see the result
play();
